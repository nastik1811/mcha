import sympy as sp  # final
import numpy as np
import matplotlib.pyplot as plt
import math

plt.style.use('ggplot')


def check_interval_correctness(func, a, b):
    return func(a) * func(b) < 0


def draw_graph(func1, x1: float, x2: float):
    fig, ax = plt.subplots()
    x = np.linspace(x1, x2)
    ax.plot(x, func1(x))
    ax.set(xlabel='Axis X', ylabel='Axis Y', title='Plot')
    plt.show()


def method_сhord(func, expr, x1, x2, eps):  # добавить проверку интервала
    if not check_interval_correctness(func, x1, x2):
        raise ValueError("MC. Incorrect interval")

    x = sp.symbols('x')
    a, b = x1, x2  # выпукла вниз
    if func(x1) * sp.diff(expr, x, x).subs(x, x1):
        a, b = b, a  # выпукла вверх

    while True:
        x_new = a - (b - a) * func(a) / (func(b) - func(a))  # из уравнения
        if abs(x_new - a) < eps:  # возможно искать пока значения функции в точке - 0 < eps
            break
        a = x_new

    return x_new


def method_targent(func, expr, x1, x2, eps):  # добавить проверку интервала
    if not check_interval_correctness(func, x1, x2):
        raise ValueError("MT. Incorrect interval")

    x = sp.symbols('x')
    x0 = x2
    if func(x1) * sp.diff(expr, x, x).subs(x, x1):
        x0 = x1

    diffed_func = sp.lambdify(x, sp.diff(expr, x), "numpy")
    #     if not (abs(func(x0) * sp.diff(x + sp.log(4 * x) - 1, x, x).subs(x, x0)) < diffed_func.subs(x, x0) ** 2):
    #         raise ValueError("Targent method don't converge for this equation")
    while True:
        x_new = x0 - func(x0) / diffed_func(x0)  # what's about division by zero
        if abs(x_new - x0) < eps:
            break
        x0 = x_new

    return x_new


def nonlinear_equation(eps):  # 1st task
    print('\tNonlinear equation')
    x = sp.symbols('x')
    expr = sp.cos(1+0.2 * x**2) - x
    func = sp.lambdify(x, expr, "numpy")
    x1 = -1
    x2 = 1
    draw_graph(func, x1, x2)
    print("Chords method's solution:", method_сhord(func, expr, x1, x2, eps))
    print("Targent method's solution:", method_targent(func, expr, x1, x2, eps))


# Functions for system
def jacobian(expr1, expr2, x0, y0):
    x, y = sp.symbols('x y')
    J = np.empty((2, 2), dtype=float)
    J[0][0] = sp.diff(expr1, x).subs([(x, x0), (y, y0)]).evalf()
    J[0][1] = sp.diff(expr1, y).subs([(x, x0), (y, y0)]).evalf()
    J[1][0] = sp.diff(expr2, x).subs([(x, x0), (y, y0)]).evalf()
    J[1][1] = sp.diff(expr2, y).subs([(x, x0), (y, y0)]).evalf()

    return J


def newton_method(expr1, expr2, f1, f2, x0, y0, eps):
    J = np.linalg.inv(jacobian(expr1, expr2, x0, y0))
    if not np.linalg.det(J):
        raise ValueError('Jacobian equals to 0')

    x_y = np.array([x0, y0])
    steps_counter = 0
    while True:
        steps_counter += 1
        x_new = x_y[0] - J[0].dot(np.array([f1(*x_y), f2(*x_y)]))
        y_new = x_y[1] - J[1].dot(np.array([f1(*x_y), f2(*x_y)]))
        if np.absolute(np.array([x_new, y_new]) - x_y).max() < eps:
            break
        x_y[0] = x_new
        x_y[1] = y_new

    print("Number of iterations in Newton method: {}".format(steps_counter))
    return (x_new, y_new)


def newton_modified_method(expr1, expr2, f1, f2, x0, y0, eps):
    x_y = np.array([x0, y0])
    steps_counter = 0
    while True:
        steps_counter += 1
        J = jacobian(expr1, expr2, *x_y)
        if not np.linalg.det(J):
            raise ValueError('Jacobian equals to 0')
        J = np.linalg.inv(J)
        func_values = np.array([f1(*x_y), f2(*x_y)])
        x_new = x_y[0] - J[0].dot(func_values)
        y_new = x_y[1] - J[1].dot(func_values)
        if np.absolute(np.array([x_new, y_new]) - x_y).max() < eps:
            break
        x_y[0] = x_new
        x_y[1] = y_new

    print("Number of iterations in Newton modified method: {}".format(steps_counter))
    return x_new, y_new


def msi(x0, y0, eps):  # manually found phi(xk). Change
    x, y = sp.symbols('x y')
    expr1 = sp.sin(x + y) / 1.6
    expr2 = sp.sqrt(1 - x ** 2)
    if x0 > 1:  # for sqrt (1 - x ** 2)
        raise ValueError("MSI. x cann't be > 1")
    J = jacobian(expr1, expr2, x0, y0)
    if np.linalg.norm(J) >= 1:
        raise ValueError("MSI don't converge")

    f_x = sp.lambdify((x, y), expr1, "numpy")
    f_y = sp.lambdify((x, y), expr2, "numpy")
    steps_counter = 0
    while True:
        steps_counter += 1
        x_new = f_x(x0, y0)
        y_new = f_y(x0, y0)
        if max([abs(x0 - x_new), abs(y0 - y_new)]) < eps:
            break
        x0, y0 = x_new, y_new

    print("Number of iterations in MSI: {}".format(steps_counter))
    return x_new, y_new


def system_nonlinear_equations(eps):
    print('\n\tSystem of nonlinear equations')
    x, y = sp.symbols('x y')
    expr1 = sp.sin(x + y) - 1.6 * x
    expr2 = x ** 2 + y ** 2 - 1
    f1 = sp.lambdify((x, y), expr1, "numpy")
    f2 = sp.lambdify((x, y), expr2, "numpy")
    p1 = sp.plotting.plot_implicit(sp.Eq(expr1, 0), (x, -4, 4), (y, 0, 4))
    p2 = sp.plotting.plot_implicit(sp.Eq(expr2, 0), (x, -4, 4), (y, 0, 4))
    p1.extend(p2)
    p1.xlabel = 'x'
    p1.ylabel = 'y'
    p1.show()
    x0, y0 = 0.5, 0.5  # initial points coordinates
    print("Newton method's solution: x = {0} y = {1}".format(*newton_method(expr1, expr2, f1, f2, x0, y0, eps)))
    print("Newton modified method's solution: x = {0} y = {1}" \
          .format(*newton_modified_method(expr1, expr2, f1, f2, x0, y0, eps)))
    print("Method simple iteration's solution: x = {} y = {}".format(*msi(x0, y0, eps)))


def main():
    eps = 0.001
    nonlinear_equation(eps)
    system_nonlinear_equations(eps)


if __name__ == '__main__':
    try:
        main()
    except ValueError as ve:
        print(ve)
    except Exception as ex:
        print(ex)