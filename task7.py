# e ^ (-sqrt(x)), a= 1, b = 4
# y' + y = 0.5xy^2, y(0) = 2, a = 0, b =2

#  1. Найти шаг интегрирования h для вычисления интеграла  по формуле трапеций с точностью 0.001.
# 2. Вычислить интеграл по формуле трапеций с шагами 2h и h. Дать уточненную оценку погрешности.
# 3 Вычислить интеграл по формуле Симпсона с шагами 2h и h. Дать уточненную оценку погрешности.
# 	4 Вычислить определенный интеграл по формуле Ньютона–Лейбница.
# 	Сравнить приближенные значения интеграла с точными. Какая формула численного интегрирования дала более точный результат?
# 	Указание для выполнения пунктов 1-4. Шаг h следует выбирать с учетом дополнительного условия:
# 	отрезок интегрирования должен разбиваться на число частей, кратное 4.
import math
import numpy as np
import sympy as sp
from prettytable import PrettyTable
import matplotlib.pyplot as plt


def pretty_table(fields: list, values):
    t = PrettyTable()
    t.field_names = fields
    for row in values:
        t.add_row(row)
    
    print(t)
    

def trapezium_method(func, a, b, n):
    h = (b - a) / n
    return h * sum([(func(a + h * i) + func(a + h * (i + 1))) / 2 for i in range(n)])


def integration_steps_amount(func, p, eps):
    n = 4
    while True:
        delta = math.fabs(trapezium_method(func, a, b, n) - trapezium_method(func, a, b, n // 2))
        if delta / (2 ** p - 1) < eps:
            break
        n += 4
    
    return n


def rough_error_estimation(expr, a, b, h, flag):
    # функции 2 и 4 производных убывающие. Максимальное значение на левой конце отрезка

    if flag == 2:
        derivative = sp.lambdify(x, sp.diff(expr, x, x))
        M = derivative(a)
        R = M * math.fabs(b - a) * h ** 2 / 12
        
    elif flag == 4:
        derivative = sp.lambdify(x, sp.diff(expr, x, x, x, x))
        M = derivative(a)
        R = M * math.fabs(b - a) * h ** 4 / 180
    return R


def simpson_method(func, a, b, n):
    h = (b - a) / n
    return h / 3 * sum(
        [func(a + h * i) + 4 * func(a + h * (i + 1)) + func(a + h * (i + 2)) for i in range(0, n - 1, 2)])


def newton_leibniz_method(expr, a, b):
    F = sp.lambdify(x, sp.integrate(expr, x))
    
   # return F.subs(x, b) - F.subs(x, a)
    return F(b) - F(a)


def compute_integral(expr, a, b):
    func = sp.lambdify(x, expr)
    eps = 0.001
    
    n = integration_steps_amount(func, 2, eps)
    h = (b - a) / n
    
    t1 = trapezium_method(func, a, b, n).__round__(6)
    t2 = trapezium_method(func, a, b, n // 2).__round__(6)
    
    r1 = rough_error_estimation(expr, a, b, h, flag=2).__round__(6)
    
    s1 = simpson_method(func, a, b, n).__round__(6)
    s2 = simpson_method(func, a, b, n // 2).__round__(6)
    r2 = rough_error_estimation(expr, a, b, h, flag=4).__round__(6)

    I = newton_leibniz_method(expr, a, b)

    pretty_table(['Method', 'h', '2h', 'Error R <='], zip(
        ['Trapezium', 'Simpson', 'Newton-Leibniz'],
        [t1, s1, I],
        [t2, s2, ''],
        [r1, r2, '']))


def ode_steps_amount(func, a, b, x0, y0, eps):
    n = 2
    while True:
        _, y1 = runge_kutta_method(func, a, b, x0, y0, n)
        _, y2 = runge_kutta_method(func, a, b, x0, y0, n // 2)
        delta = math.fabs(y2[1] - y1[2])
        if delta / 15 < eps:
            break
        n *= 2
    
    return n


def euler_method(func, a, b, x0, y0, n):
    if x0 != a:
        raise ValueError('Must be equal')
        
    h = (b - a) / n
    x = np.empty(n + 1, float)
    y = np.empty(n + 1, float)
    x[0] = x0
    y[0] = y0
    
    for i in range(n):
        y[i + 1] = y[i] + h * func(x[i], y[i])
        x[i + 1] = x[i] + h
    
    return x, y.round(6)


def runge_kutta_method(func, a, b, x0, y0, n):
    if x0 != a:
        raise ValueError('Must be equal')
    
    h = (b - a) / n
    x = np.empty(n + 1)
    y = np.empty(n + 1)
    x[0] = x0
    y[0] = y0
    
    for i in range(n):
        k1 = func(x[i], y[i])
        k2 = func(x[i] + h / 2, y[i] + k1 * h / 2)
        k3 = func(x[i] + h / 2, y[i] + k2 * h / 2)
        k4 = func(x[i] + h, y[i] + h * k3)
        x[i + 1] = x[i] + h
        y[i + 1] = y[i] + h / 6 * (k1 + k4 + 2 * (k2 + k3))
    
    return x, y.round(6)


def adams_method(f, a, b, x0, y0, n):
    if x0 != a:
        raise ValueError('Must be equal')
    
    h = (b - a) / n
    x = np.empty(n + 1)
    y = np.empty(n + 1)
    x[0] = x0
    y[0] = y0
    x[1] = x[0] + h
    y[1] = y[0] + h * f(x[0], y[0])
    for i in range(1, n):
        predictor = y[i] + h / 2 * (3 * f(x[i], y[i]) - f(x[i - 1], y[i - 1]))
        x[i + 1] = x[i] + h
        y[i + 1] = y[i] + h / 2 * (f(x[i], y[i]) + f(x[i + 1], predictor))
    
    return x, y.round(6)

    
def solve_cauchy_problem(expr, a, b, y0):

    x, y = sp.symbols('x y')
    diff_func = sp.lambdify((x, y), expr)
    x0 = a
    x, y = sp.symbols('x y')
    func = sp.lambdify((x, y), expr, "numpy")
    eps = 0.0001
    n = ode_steps_amount(func, a, b, x0, y0, eps)
    
    # Метод Рунге-Кутта

    x_r1, y_r1 = runge_kutta_method(diff_func, a, b, x0, y0, n)
    x_r2, y_r2 = runge_kutta_method(diff_func, a, b, x0, y0, n // 2)

    print('\nRunge-Kutta method')
    pretty_table(['x[i]', 'y[i]', '~x[i]', '~y[i]', 'delta[i]'],
                 zip(x_r1,
                     y_r1,
                     [x_r2[i // 2] if i & 1 == 0 else "" for i in range(n + 1)],
                     [y_r2[i // 2] if i & 1 == 0 else "" for i in range(n + 1)],
                     [abs(y_r2[i // 2] - y_r1[i]).round(6) if i & 1 == 0 else "" for i in range(n + 1)]))

    plt.plot(x_r1, y_r1, label='Runge-Kutta')
    
    # Метод Адамса
    x_a1, y_a1 = adams_method(diff_func, a, b, x0, y0, n)
    x_a2, y_a2 = adams_method(diff_func, a, b, x0, y0, n // 2)

    print('\nAdams method')
    pretty_table(['x[i]', 'y[i]', '~x[i]', '~y[i]', 'delta[i]'],
                 zip(x_a1,
                     y_a1,
                     [x_a2[i // 2] if i & 1 == 0 else "" for i in range(n + 1)],
                     [y_a2[i // 2] if i & 1 == 0 else "" for i in range(n + 1)],
                     [abs(y_a2[i // 2] - y_a1[i]).round(6) if i & 1 == 0 else "" for i in range(n + 1)]))
    
    plt.plot(x_a1, y_a1, label='Adams')
    
    # Метод Эйлера

    x_e1, y_e1 = euler_method(diff_func, a, b, x0, y0, n)
    x_e2, y_e2 = euler_method(diff_func, a, b, x0, y0, n // 2)

    print('\nEuler method')
    pretty_table(['x[i]', 'y[i]', '~x[i]', '~y[i]', 'delta[i]'],
                 zip(x_e1,
                     y_e1,
                     [x_e2[i // 2] if i & 1 == 0 else "" for i in range(n + 1)],
                     [y_e2[i // 2] if i & 1 == 0 else "" for i in range(n + 1)],
                     [abs(y_e2[i // 2] - y_e1[i]).round(6) if i & 1 == 0 else "" for i in range(n + 1)]))
    
    plt.plot(x_e1, y_e1, label='Euler')
    
    # rough solution
    solution = sp.lambdify(x, 2 / (x + 1))

    plt.plot(np.linspace(a, b), solution(np.linspace(a, b)), label='Rough solution')
    
    plt.legend()
    plt.grid()
    plt.show()
    
    h = (b - a) / n
    x_range = np.arange(a, b + h, h)
    y_rough = np.array([solution(xi) for xi in x_range]).round(6)
    d1 = abs(y_rough - y_r1)
    d2 = abs(y_rough - y_a1)
    
    print('Comparison table')
    pretty_table(['xi', 'yi', 'runge-kutta', 'd1', 'adams', 'd2'], zip(
        x_range,
        y_rough,
        y_r1,
        d1.round(6),
        y_a1,
        d2.round(6)))


if __name__ == '__main__':
    x, y = sp.symbols('x y')
    expr = sp.exp(-sp.sqrt(x))
    print()
    a = 1
    b = 4
    compute_integral(expr, a, b)
    
    print("ODE solution")
    a = 0
    b = 2
    diff = 0.5 * x * y ** 2 - y
    y0 = 2
    solve_cauchy_problem(diff, a, b, y0)