import sympy as sp  # final
import numpy as np
import matplotlib.pyplot as plt
import math


def main():
    x, y = sp.symbols('x y')
    eps = 0.001
    nonlin_expr = sp.cos(1 + 0.2 * x ** 2) - x

    print('\tNonlinear equation:', end='\t')
    print('cos(1 + 0.2 * x^2) - x')
    solve_nonlinear_equation(nonlin_expr, eps)

    expr1 = sp.sin(x + 2 * y) - 1.2 * x
    expr2 = x ** 2 + y ** 2 - 1

    print('\n\tSystem of nonlinear equations:')
    print('sin(x + 2y) - 1.2x')
    print('x^2 + y^2 = 1')
    solve_system(expr1, expr2, eps)


def check_range_correctness(func, a, b):
    return func(a) * func(b) < 0


def nonlinear_func(x):
    return np.cos(1 + 0.2 * x ** 2) - x


def make_func(expr, flag=None):
    x = sp.symbols('x')
    if flag == "diff":
        expr = sp.diff(expr, x)
    if flag == "diff2":
        expr = sp.diff(expr, x, x)
    return sp.lambdify(x, expr, "numpy")


def make_two_param_func(expr, flag = None, derivable_arg = None):
    x, y = sp.symbols('x y')
    if flag == "diff":
        d = sp.symbols(derivable_arg)
        expr = sp.diff(expr, d)
    return sp.lambdify((x, y), expr, "numpy")


def plot_equation(func):
    x = np.arange(-1.5, 1.5, 0.1)
    y = func(x)
    ax = plt.axes()
    ax.plot(x, y)
    ax.plot([-1.5, 1.5], [0, 0])
    ax.set(xlabel='X', ylabel='Y', title='Graphic of nonlinear equation.')
    plt.show()


def plot_system(expr1, expr2, x1, y1):
    x, y = sp.symbols('x y')
    p1 = sp.plotting.plot_implicit(sp.Eq(expr1, 0), (x, 0, x1), (y, 0, y1))
    p2 = sp.plotting.plot_implicit(sp.Eq(expr2, 0), (x, 0, x1), (y, 0, y1))
    p1.extend(p2)
    p1.xlabel = 'X'
    p1.ylabel = 'Y'
    p1.show()


def is_concave_up(a, expr):
    x = sp.symbols('x')
    return make_func(expr)(a) * make_func(expr, flag="diff2")(a) < 0


def is_targent_method_converges(expr, x0):
    func = make_func(expr)
    derivative = make_func(expr, flag="diff")
    second_derivative = make_func(expr, flag="diff2")
    return abs(func(x0) * second_derivative(x0)) < derivative(x0) ** 2


def solve_nonlinear_equation(nonlin_expr, eps):
    nonlin_func = make_func(nonlin_expr)
    plot_equation(nonlin_func)
    a, b = tuple(map(float, input("Enter range of search:").split()))

    if check_range_correctness(nonlin_func, a, b):
        print("Solution using chords: x = {}".format(solve_with_method_of_chords(nonlin_expr, a, b, eps)))
        print("Solution using targets: x = {}".format(solve_with_method_tangent(nonlin_expr, a, b, eps)))
    else:
        print("There are no roots in this range.")


def solve_with_method_of_chords(expr, a, b, eps):
    func = make_func(expr)
    if is_concave_up(a, expr):
        a, b = b, a
    while True:
        x_new = a - (b - a) * func(a) / (func(b) - func(a))
        if abs(x_new - a) < eps:
            break
        a = x_new
    return x_new.round(4)


def solve_with_method_tangent(expr, a, b, eps):
    func = make_func(expr)
    if is_concave_up(a, expr):
        x0 = b
    else:
        x0 = a

    derivative = make_func(expr, flag="diff")
    if is_targent_method_converges(expr, x0):
        while True:
            x_new = x0 - func(x0) / derivative(x0)
            if abs(x_new - x0) < eps:
                break
            x0 = x_new
        return x_new.round(4)
    else:
        print ("Tangent method doesn't converge.")
        return None


def solve_system(expr1, expr2, eps):
    plot_system(expr1, expr2, 1.5, 1.5)

    x0, y0 = tuple(map(float, input("Enter an initial point:").split()))
    print("Solution using simple iteration: x = {} y = {}\n".format(*msi(x0, y0, eps)))
    print("Solution using Newton method: x = {0} y = {1}\n".format(*newton_method(expr1, expr2, x0, y0, eps)))
    print("Solution using Newton modified method: x = {0} y = {1}".format(*newton_modified_method(expr1, expr2, x0, y0, eps)))


def newton_method(expr1, expr2, x0, y0, eps):
    initial_point = np.array([x0, y0])
    f1 = make_two_param_func(expr1)
    f2 = make_two_param_func(expr2)

    steps = 0
    while True:
        steps += 1
        try:
            J_inv = np.linalg.inv(jacobian(expr1, expr2, *initial_point))
        except ZeroDivisionError:
            print("Jacobian equals to 0")
            return -1
        F = np.array([f1(*initial_point), f2(*initial_point)])
        x_new = initial_point[0] - J_inv[0].dot(F)
        y_new = initial_point[1] - J_inv[1].dot(F)
        if np.absolute(np.array([x_new, y_new]) - initial_point).max() < eps:
            break
        initial_point[0] = x_new
        initial_point[1] = y_new

    print("Number of iterations in Newton method: {}".format(steps))
    return x_new.round(4), y_new.round(4)


def jacobian(expr1, expr2, x0, y0):

    J = np.empty((2, 2), dtype=float)
    J[0][0] = make_two_param_func(expr1, flag="diff", derivable_arg="x")(x0, y0)
    J[0][1] = make_two_param_func(expr1, flag="diff", derivable_arg="y")(x0, y0)
    J[1][0] = make_two_param_func(expr2, flag="diff", derivable_arg="x")(x0, y0)
    J[1][1] = make_two_param_func(expr2, flag="diff", derivable_arg="y")(x0, y0)
    return J


def newton_modified_method(expr1, expr2, x0, y0, eps):
    initial_point = np.array([x0, y0])
    f1 = make_two_param_func(expr1)
    f2 = make_two_param_func(expr2)
    try:
        J_inv = np.linalg.inv(jacobian(expr1, expr2, *initial_point))
    except ZeroDivisionError:
        print("Jacobian equals to 0")
        return -1

    steps = 0
    while True:
        steps += 1
        F = np.array([f1(*initial_point), f2(*initial_point)])
        x_new = initial_point[0] - J_inv[0].dot(F)
        y_new = initial_point[1] - J_inv[1].dot(F)
        if np.absolute(np.array([x_new, y_new]) - initial_point).max() < eps:
            break
        initial_point[0] = x_new.round(4)
        initial_point[1] = y_new.round(4)

    print("Number of iterations in Modified Newton method: {}".format(steps))
    return initial_point


def msi(x0, y0, eps):
    x, y = sp.symbols('x y')
    expr1 = sp.sin(x + 2*y) / 1.2
    expr2 = sp.sqrt(1 - x ** 2)
    if x0 > 1:  # for sqrt (1 - x ** 2)
        raise ValueError("Error in MSI: x > 1")

    f1 = make_two_param_func(expr1)
    f2 = make_two_param_func(expr2)

    steps= 0
    while True:
        steps += 1
        x_new = f1(x0, y0)
        y_new = f2(x0, y0)
        if max([abs(x0 - x_new), abs(y0 - y_new)]) < eps:
            break
        x0, y0 = x_new, y_new
        if steps > 20:
            print("MSI doesn't converge")
            return None, None

    print("Number of iterations in MSI: {}".format(steps))
    return x_new.round(4), y_new.round(4)
    



if __name__ == '__main__':
    try:
        main()
    except ValueError as ve:
        print(ve)
    except Exception as ex:
        print(ex)
