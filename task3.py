import numpy as np
import math


def find_diagonal_matrix(A):
    U = np.zeros_like(A)
    try:
        for i in range(len(A)):
            for j in range(i, len(A)):
                if i == j:
                    U[i][i] = math.sqrt(A[i][i] - sum([U[k][i] ** 2 for k in range(0, i)]))
                else:
                    U[i][j] = (A[i][j] - sum([U[k][i] * U[k][j] for k in range(0, i)])) / U[i][i]
    except ValueError:
        print("Given matrix is not positive definite.")
        return None
    return U


def solve_with_sqrt_method(U, b):
    y = np.matmul(np.linalg.inv(U.transpose()), b)
    x = np.matmul(np.linalg.inv(U), y)
    return x


def get_determinant(A):
    U = find_diagonal_matrix(A)
    return np.prod([U[i][i] ** 2 for i in range(0, len(A))])


def get_inverted_matrix(A, U):
    E = np.eye(len(A))
    A_inv = np.zeros_like(A)
    for i in range(len(A)):
        A_inv[i] = solve_with_sqrt_method(U, E[i])
    return A_inv


def print_result(X):
    for i in range(X.size):
        print('X[{0}] = '.format(i + 1), "%1.3f" % (X[i]))


if __name__ == "__main__":
    A = np.array([
        [-3, 1, 1],
        [1, -4, 1],
        [1, 1, -5]
    ], dtype=float)
    b = np.array(
        [1, -1, 1], dtype=float
    )
    is_symmetric = True
    for i in range(len(A)):
        for j in range(len(A)):
            if A[i][j] != A[j][i]:
                is_symmetric = False
    if not is_symmetric:
        A = np.matmul(A.transpose(), A)
        b = np.matmul(A.transpose(), b)

    U = find_diagonal_matrix(A)
    if U is None:
        print("Can't find a solution")
    else:
        print_result(solve_with_sqrt_method(U, b))
        print('---------------------')
        print('Determinant calculated with sqrt method:')
        print(get_determinant(A))
        print('\nNumPy determinant: ')
        print(np.linalg.det(A))
        print('\n----- Inverted matrix -------')
        print(get_inverted_matrix(A, U).round(3))
        print('\n--- NumPy inverted matrix ---')
        print(np.linalg.inv(A).round(3))
