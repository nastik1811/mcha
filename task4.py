import numpy as np
import math


def make_symmetric(A, b):
    A = np.matmul(A.transpose(), A)
    b = np.matmul(A.transpose(), b)


def get_position_of_max(A):
    n = len(A)
    i_max = 0
    j_max = 1
    current_max = math.fabs(A[i_max][j_max])
    for i in range(n - 1):
        for j in range(i + 1, n):
            if math.fabs(A[i][j]) > current_max:
                current_max = A[i][j]
                i_max = i
                j_max = j
    return i_max, j_max


def get_engine_vectors(A, eps):
    n = len(A)
    U = np.eye(n)
    i, j = get_position_of_max(A)
    while math.fabs(A[i][j]) > eps:
        phi = math.atan(2 * A[i][j] / (A[i][i] - A[j][j])) / 2
        H = np.eye(n)
        H[i][i] = H[j][j] = math.cos(phi)
        H[i][j] = - math.sin(phi)
        H[j][i] = math.sin(phi)
        U = np.matmul(U, H)
        A = np.matmul(np.matmul(np.transpose(H), A), H)
        i, j = get_position_of_max(A)

    engine_values = [round(A[k][k], 3) for k in range(n)]
    engine_vectors = [U.transpose().round(3)[k] for k in range(n)]
    return engine_values, engine_vectors


def print_values(X):
    for i in range(X.size):
        print('[{0}] = '.format(i + 1), "%1.4f" % (X[i]))


if __name__ == "__main__":
    A = np.array([
        [3, -1, -1],
        [-1, 4, -1],
        [-1, -1, 5]
    ], dtype=float)
    b = np.array(
        [1,-1, 1], dtype=float
    )
    A = np.matmul(A.transpose(), A)
    n = len(A)
    U = np.eye(n)
    eps = 0.001
    engine_values, engine_vectors = get_engine_vectors(A, eps)
    print('\nNumPy engine values: ')
    print(np.linalg.eigvalsh(A).round(3))
    print('\nCalculated engine values: ')
    print(engine_values)
    print('\nCalculated engine vectors: ')
    for i in range(len(engine_vectors)):
        print(' e%d = ' % (i + 1), engine_vectors[i])

    for i in range(len(engine_values)):
        print(np.matmul(A, engine_vectors[i]))
        print(engine_values[i] * engine_vectors[i])




